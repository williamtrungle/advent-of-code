# Advent of Code
An annual [coding challenge](https://adventofcode.com/) during the month of December presenting 2 challenges a day for 25 days.

## Available Years

- [2019](https://gitlab.com/williamtrungle/advent-of-code/tree/2019)
